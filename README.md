<?xml version="1.0" encoding="utf-8"?>
<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:html="http://www.w3.org/1999/xhtml" viewBox="0 0 1366 706" onload='alert("script!")' version="1.1" width="12cm" height="12cm">
  
      <circle cx="6cm" cy="2cm" r="100" style="fill:red;"
                     transform="translate(0,50)" />
     <circle cx="6cm" cy="2cm" r="100" style="fill:blue;"
                     transform="translate(70,150)" />
     <circle cx="6cm" cy="2cm" r="100" style="fill:green;"
                     transform="translate(-70,150)"></circle>
  
</svg>